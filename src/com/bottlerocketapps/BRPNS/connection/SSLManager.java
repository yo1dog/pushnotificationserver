package com.bottlerocketapps.BRPNS.connection;

import java.io.IOException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SSLManager
{
	private static final char[] KEYSTORE_PASSPHRASE = "111111".toCharArray();
	
	private static final String APNS_SERVER_HOST = "gateway.sandbox.push.apple.com";
	private static final int    APNS_SERVER_PORT = 2195;
	
	private static SSLSocketFactory socketFactory = null;
	
	public static SSLSocketFactory getSocketFactory() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException, KeyManagementException
	{
		if (socketFactory != null)
			return socketFactory;
		
		// create KeyStore
		KeyStore keyStore = KeyStore.getInstance("PKCS12"); //.p12 type
		
		// loads the p12 file into the KeyStore
		keyStore.load(
				Thread.currentThread().getContextClassLoader().getResourceAsStream("../../META-INF/cert/Certificates.p12"), // relative to webapps/br_everyone/WEB-INF/classes/
				KEYSTORE_PASSPHRASE);
		
		System.out.println("created KeyStore");
		
		// create KeyManagerFactory
		KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
		keyManagerFactory.init(keyStore, KEYSTORE_PASSPHRASE);
		
		System.out.println("created KeyManagerFactory");
		
		// create KeyManagers
		KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();
		
		// create SSLContext
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(keyManagers, null, null);
		
		System.out.println("created sslContext");
		
		socketFactory = sslContext.getSocketFactory();
		return socketFactory;
	}
	
	public static SSLSocket getSocket() throws UnknownHostException, IOException, UnrecoverableKeyException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException, CertificateException
	{
		SSLSocketFactory socketFactory = getSocketFactory();
		return (SSLSocket)socketFactory.createSocket(APNS_SERVER_HOST, APNS_SERVER_PORT);
	}
}
