package com.bottlerocketapps.BRPNS.endpoints;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;

import javax.net.ssl.SSLSocket;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bottlerocketapps.BRPNS.connection.SSLManager;

/**
 * Servlet implementation class Test
 */
@WebServlet("/test")
public class Test extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try {
			Push.alert("Hello World!",
					getServletContext().getRealPath("/META-INF/cert/CertificatesBad.p12"),
					"111111", false, "Your token");
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeystoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*try {
			SSLSocket socket = SSLManager.getSocket();
			
			try
			{
				socket.getOutputStream().write("hello!".getBytes());
				socket.getOutputStream().flush();
				
				for (int i = 0; i < 3; i++)
				{
					byte[] buff = new byte[1204];
					
					if (socket.isConnected())
						System.out.println("is connected");
					
					socket.getInputStream().read(buff);
					
					
					
					System.out.println("out: " + new String(buff));
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			finally
			{
				socket.close();
			}
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//socket.getOutputStream().write(bytes);*/
	}

}
